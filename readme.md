## **Recap des notios du Mercredi 9 Mars**

**1) Classe**

En PHP Orienté objet, la classe représentera le moule qui nous permettra de créer un objet. Elle contient les mèthodes et les attributs d'un objet.
En PHP on utilise le mot clé class puis le nom de la classe. Par convention les noms de classes seront en UpperCamelcase (ChaiseLongue).

````php
<?php
    class NomDeLaClasse {
    }

````

**2) Objet / Créer un objet**

Pour créer un objet en PHP on utilise le mot clé new. 

````php
    // Ici je cré une nouvelle instance d'objet NomDeMaClasse
    // Je le stock dans une variable ($obj)
    $obj = new NomDeMaClasse();
````

Si on a un constructeur dans notre classe : 

````php
<?php
    class NomDeLaClasse {
        
        private $attribut;
        
        public function __construct($attribut) {
            $this->attribut = $attribut;
        }
    
    }

````

Pour instancier cet objet, il faudra passer mon $attribut dans le new 

````php
   
    $obj = new NomDeMaClasse("Mon super attribut !");
````

**3) Méthode magique** 

Méthode qui commence en PHP par un double underscore __
Les méthodes magiques sont appelés automatiquement (pas besoin de les appeler). 
On a vu pour le moment le constructeur et le destructeur

````php
   class MaClasse {
    // Nous avons ici un constructeur, il sera appelé 
    // quand nous allons instancier un nouvel objet avec le mot clé new
    public function __construct(){
    
    }
    
    // Cette méthode est appelé lorsque notre objet est supprimé
    // Nos objets sont tous supprimés à la fin du script (permet de libérer la mémoire)
    public function __detruct(){
    
    }
   }

````

4) Attribut
   
Pour créer un attribut sur un objet on commence par indiquer la visibilité 

public : Accessible depuis notre objet et aussi en dehors
private : Accessible que depuis notre objet 
protected : Accessible depuis notre objet mais aussi depuis les objets qui en héritent
   
Réprésente les caractéristiques de notre objet (couleur d'une voiture, marque ...)
````php
    class MaClasse{
         // On indique la visibilité (public, protected, privé)
        // Notre attribut précédé par un $
        private $attribut;
    }
   
````

**5) Méthode**

Représente une fonctionnalité de notre objet  (Accélérer, Freiner ...)

On indique sa visibilité suivi du mot clé function, on indique le nom de la fonction on passe les paramétres requis si besoin entre parenthèse ()

````php
<?php

class MaClasse {
    // Méthode public (accessible) qui permet à un objet de type MaClaqse
    // d\'afficher un message. 
    public function displayMessage($message){
        echo($message);
    }
}
````

7) Les accesseurs / mutateurs 

Réprésente les getteurs / setteurs. 

Il permettent L'accession (getteurs) et la mise à jour (setteurs d'attribut privé ou inaccessible).
Ce sont des méthodes publiques. 

````php
class MaClasse{
    private $attributPrive;
    
    // Une méthode publique qui retourne la valeur de mon attribut privé
    public function getAttributPrive(){
        return $this->attributPrive;
    }
    
    // Une méthode publique qui met à jour la valeur d'un attribut privé
    public function setAttributPrive($attribut){
        $this->attributPrive = $attribut;
    }
}

````

**8) Héritage**

Permet à un objet de reprendre les attributs et méthodes d'un objet parent 
Une classe parente peut avoir autant d'enfant que l'on souhaite
Une classe enfant ne peut avoir qu'une seule classe parent 

Il faut veiller à ne pas avoir de classe enfant qui ont le même nom du parent. 
Sinon la méthode ou l'attribut de la classe parent sera écrasé 
On peut tout de même avoir le même nom de méthode et utiliser le mot clé parent::nomDeLaMethode() pour executer a la fois la méthode du parent et celle de l'enfant 

En PHP, on utilise le mot clé extents pour réaliser de l'héritage

`````php
<?php

class ClasseParent{
    public function __construct(){
        echo('Construction du parent !');
    }
}

class Enfant extends ClasseParent {
    public function __construct(){
        parent::__construct();
        echo('Construction de l\'enfant');
    }
}

`````

Attribut protected 

Permet d'autoriser l'accès à un attribut ou à une méthode au sein de l'objet ou au sein des objets qui en héritent

