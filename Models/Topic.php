<?php
    class Topic {
        private $id;
        private $author;
        private $dateAdd;
        private $titre;
        private $image;
        private $contenu;

        public function __construct($id, $author, $titre, $image, $contenu){
            $this->id = $id;
            $this->author = $author;
            $this->dateAdd = date_create('now');
            $this->titre = $titre;
            $this->image = $image;
            $this->contenu = $contenu;
        }

        public function getId(){
            return $this->id;
        }

        public function getTitre(){
            return $this->titre;
        }

        public function getAuthor(){
            return $this->author;
        }

        public function getDateAdd(){
            return $this->dateAdd;
        }

        public function getImage()
        {
            return $this->image;
        }

        public function getContenu()
        {
            return $this->contenu;
        }

        public function setAuthor($author){
            $this->author = $author;
        }

        public function setTitre($nouveauTitre){
            $this->titre = $nouveauTitre;
        }

        public function setImage($image){
            $this->image = $image;
        }

        public function setContenu($contenu){
            $this->contenu = $contenu;
        }

        public function setDateAdd($dateAdd): void
        {
            $this->dateAdd = $dateAdd;
        }
    }
?>