<?php
// On étend de DbManager pour avoir accès à la connexion à notre BDD
class TopicManager extends DbManager {
    // Ici j'utilise l'attribut BDD du parent pour transformer une requête MySQL en tableau d'objet
    // Ainsi je réccupére un tableau d'objet plus facile à manipuler qu'un tableau MySQL classique
    public function getAll()
    {
        $objectArray = [];
        $query = $this->bdd->prepare("SELECT * FROM topic");
        $query->execute();
        $resultats = $query->fetchAll();

        foreach ($resultats as $resultat) {
            $objectArray[] = new Topic($resultat["id"], $resultat["author"], $resultat["titre"], $resultat["image"], $resultat["contenu"]);
        }

        return $objectArray;
    }

    // Cette méthode prend en paramètre un ID
    // Elle génére une requête MySQL qui réccupére la ligne dans la BDD
    // Elle retourne l'objet qui a cet id
    public function getOne($id){
        $result = null;

        $req = $this->bdd->prepare("SELECT * FROM topic WHERE id = :id");
        $req->execute(["id"=> $id]);
        $resultat = $req->fetch();

        return new Topic($resultat["id"], $resultat["author"], $resultat["titre"], $resultat["image"], $resultat["contenu"]);
    }

    // Cette méthode prend en paramètre un objet
    // Elle génére une requête d'insertion dans notre BDD
    public function create($objet){
        $req = $this->bdd->prepare("INSERT INTO topic ( author, date_add,titre, image, contenu ) 
        VALUES ( :author, :date_add, :titre, :image, :contenu)");

        $req->execute([
            'author'=> $objet->getAuthor(),
            'date_add'=> $objet->getDateAdd()->format('Y-m-d'),
            'titre'=> $objet->getTitre(),
            'image'=> $objet->getImage(),
            'contenu'=> $objet->getContenu()
        ]);
    }

    // Cette méthode prend en paramètre un objet
    // Elle génére une requête de mise à jour dans notre BDD
    public function edit($objet){
        $req = $this->bdd->prepare("UPDATE topic SET author = :author,
                 date_add = :date_add, titre = :titre, image = :image,
                 contenu = :contenu WHERE id = :id");

        $req->execute([
            'author'=> $objet->getAuthor(),
            'date_add'=> $objet->getDateAdd()->format('Y-m-d'),
            'titre'=> $objet->getTitre(),
            'image'=> $objet->getImage(),
            'contenu'=> $objet->getContenu(),
            'id'=> $objet->getId()
        ]);
    }

    // Prend en paramétre un objet
    // génére une requête de suppression
    public function delete($obj){
        $query = $this->bdd->prepare('DELETE FROM topic WHERE id = :id');
        $query->execute(["id"=> $obj->getId()]);
    }
}