<?php
class CategoryManager extends DbManager {

    public function getAll(){
        $arrayObjectResult = [];

        $query = $this->bdd->prepare('SELECT * FROM category');
        $query->execute();

        while ( $result = $query->fetch() ){
            $arrayObjectResult[] = new Category($result["id"], $result["label"]);
        }

        return $arrayObjectResult;

    }

    public function getOne($id){
        $query =
            $this->bdd->prepare("SELECT * FROM category WHERE id = :id");

        $query->execute(["id"=> $id]);

        $resultat = $query->fetch();

        if($resultat != false){
            $resultat = new Category($resultat["id"], $resultat["label"]);
        } else {
            $resultat = null;
        }

        return $resultat;
    }
}