<?php
// Classe qui permet de se connecter à la BDD
// On retrouve les informations de connexion
// Toute classe qui héritera de cette classe
// sera connecté à notre BDD
// Les classes enfants pourront utiliser l'attribut $bdd pour effectuer des requêtes
class DbManager{

    // L'attribut $bdd sera partagé avec les classes enfants
    protected $bdd;

    // Ces variables seront utilisé seulement dans le contructeur
    // pour se connecter à la BDD. On en aura pas besoin ailleurs (c'est pour cela qu'ils sont en visibilité privé)
    private $dbName = 'cci-forum';
    private $endPoint = 'database';
    private $password = 'tiger';
    private $user = 'root';

    // Dans le constructeur de l'objet, nous nous connectons à la BDD
    public function __construct(){
        $this->bdd = new PDO("mysql:dbname=".$this->dbName.";host=".$this->endPoint."", $this->user, $this->password);
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}