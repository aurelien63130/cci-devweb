<?php

// EN PREMIER ON REQUIRE NOS MODEL
require 'Models/Topic.php';
require 'Models/Category.php';
// ON REQUIRE NOTRE DB MANAGER (CONNEXION À LA BDD)
// ON LE FAIT AVANT LES AUTRES MANAGERS EN HERITENT
require 'Manager/DbManager.php';
// ON AJOUTE NOS MANAGERS.
require 'Manager/CategoryManager.php';
require 'Manager/TopicManager.php';






/**********************
 * ACTION
 *
 * SUR
 *
 * LES
 *
 * TOPICS
 *
 **********************/






// On cré un objet topic manager
// Le role de cet objet sera lors de la selection
// de transformer une requête SQL en objet
// Lors de l'insertion, il transforme un objet en requête SQL
$topicManager = new TopicManager();

// J'appel mon manager pour afficher toutes les représentation objet
// contenu dans notre BDD
// var_dump($topicManager->getAll());

// Je réccupére grâce à mon manager l'objet qui a l'id 1
$topicNumberOne = $topicManager->getOne(1);
// var_dump($topicNumberOne);

/**********************
 * Create
 **********************/

// Je cré un nouvel objet Topic
$newTopic = new Topic(null, 'Aurélien Delorme',
    'L\'ASM bat le LOU', 'https://france3-regions.francetvinfo.fr/image/gtzwG5fYG9iFzoXGhXpOxXi9MkU/600x400/regions/2020/06/08/5edeaf0b22740_90300_640_360.jpg',
"Super match au michelin");

// Je demande à mon manager d'aller inserer dans me BDD ce topic
// Il va prendre mon objet et le transformer en requête SQL
 // $topicManager->create($newTopic);

 /**********************
  * Edit
  **********************/

 // Je réccupére mon objet qui a l'ID 2
$objet2 = $topicManager->getOne(2);
// Je modifie son titre
$objet2->setTitre("Nouveau titre !");
// Je demande à mon manager de le mettre à jour en BDD
// $topicManager->edit($objet2);
//var_dump($objet2);

/*************************
 * DELETE
 *************************/
// $obj3 = $topicManager->getOne(3);

// var_dump($obj3);

// $topicManager->delete($obj3);



/**********************
 * ACTION
 *
 * SUR
 *
 * LES
 *
 * CATEGORIES
 *
 **********************/

$categoryManager = new CategoryManager();

$categories = $categoryManager->getAll();

var_dump($categories);


$categ1 = $categoryManager->getOne(1);
var_dump($categ1);

